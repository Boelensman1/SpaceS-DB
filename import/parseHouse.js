/**
 * Parse a house string into an object
 *
 * @param {string} house The string with the info about the house/workUnit
 * @returns {object} Object describing the house/workUnit
 */
function parseHouse(house) {
  if (!house) {
    throw Error('Every resident should have a house')
  }
  const houseSplit = house.split('.')

  if (houseSplit.length > 4) {
    throw Error(`unexpected length of house string (${house})`)
  }

  const houseParsed = {
    blockNumber: houseSplit[0],
    floorNr: houseSplit[1],
    constructionHouseNumber: houseSplit[2],
  }
  if (houseSplit[3]) {
    houseParsed.roomNr = houseSplit[3]
  }
  // addition
  const split2 = houseSplit[2].split('-')
  if (split2.length > 1) {
    houseParsed.constructionHouseNumber = split2[0]
    houseParsed.constructionHouseNumberAddition = split2[1]
  }

  return houseParsed
}

module.exports = parseHouse
