#!/usr/bin/env node

const fs = require('fs')
const loadData = require('./loadData')
const models = require('../models')
const _ = require('lodash')
const jsonfile = require('jsonfile')

const argv = require('optimist').argv._

if (argv.length === 0) {
  throw Error('Need to give a file as first argument')
}
if (argv.length > 1) {
  throw Error('Can only import one file at the time.')
}
const fileLocation = argv[0]
if (!fs.lstatSync(fileLocation).isFile()) {
  throw Error('Can not find file or it is a directory.')
}

const preparedHouseNumbersLocation = `${__dirname}/preparedHouseNumbers.json`


/**
 * Get a resident from the bundled values
 *
 * @param {object} value The values where the resident is a part of
 * @returns {object} The resident
 */
function getResident(value) {
  const resident = _.clone(value)
  delete resident.house
  delete resident.workunit
  return resident
}

/**
 * Import database
 *
 * @param {string} filename The location of the csv to be imported
 * @param {string} preparedHouseNumbersFile The location of the prepared
 *                                          house numbers file
 * @returns {undefined}
 */
function importDatabase(filename, preparedHouseNumbersFile) {
  let preparedHouseNumbers
  try {
    preparedHouseNumbers = jsonfile.readFileSync(preparedHouseNumbersFile)
  } catch (err) {
    console.error('Error while loading house numbers, ' +
      'please run preparehousenumbers first.')
    return
  }

  models.init().then(() => {
    console.log('Loading data')
    loadData(filename, preparedHouseNumbers, (data) => {
      console.log('Inserting into database')
      // insert residents
      data.forEach((value) => {
        const residentValues = getResident(value)
        models.resident.create(residentValues).then((resident) => {
          if (value.house) {
            const houseValues = value.house
            models.house.create(houseValues).then((house) => {
              resident.setHouse(house)
            })
          }
          if (value.workUnit) {
            const workUnitValues = value.workUnit
            models.workUnit.create(workUnitValues).then((workUnit) => {
              resident.setWorkUnit(workUnit)
            })
          }
        }).catch((e) => { console.log(residentValues); throw (e) })
      })
      console.log('Importing', data.length, 'residents')
    })
  })
}

importDatabase(fileLocation, preparedHouseNumbersLocation)
