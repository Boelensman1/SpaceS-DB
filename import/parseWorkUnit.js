/**
 * Parse a workUnit string into an object
 *
 * @param {string} workUnit The string with the info about the workUnit
 * @returns {object} Object describing the workUnit
 */
function parseWorkUnit(workUnit) {
  if (!workUnit) {
    return false
  }
  const workUnitSplit = workUnit.split('.')

  if (workUnitSplit.length > 3) {
    throw Error(`unexpected length of workUnit string (${workUnit})`)
  }

  const workUnitParsed = {
    blockNumber: workUnitSplit[0],
    floorNr: workUnitSplit[1],
    constructionWorkUnitNumber: workUnitSplit[2],
  }

  // addition
  const split2 = workUnitSplit[2].split('-')
  if (split2.length > 1) {
    workUnitParsed.constructionWorkUnitNumber = split2[0]
    workUnitParsed.constructionWorkUnitNumberAddition = split2[1]
  }

  return workUnitParsed
}

module.exports = parseWorkUnit
