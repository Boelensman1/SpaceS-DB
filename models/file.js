const fs = require('fs')
const defaultToJSON = require('./util/defaultToJSON')

module.exports = (sequelize, DataTypes) => {
  const File = sequelize.define('file', {
    name: {
      type: DataTypes.STRING,
      comment: 'Name of the file',
      allowNull: false,
      validate: {
        notEmpty: true,
        len: {
          args: [3, 200],
          msg: 'Should be between 3 and 200 characters.',
        },
      },
    },
    location: {
      type: DataTypes.STRING,
      comment: 'Location where the file is',
      unique: true,
      validate: {
        fileIsAccessible(location) {
          try {
            fs.accessSync(location, fs.F_OK)
          } catch (err) {
            throw new Error(
              `File ${location} is not accessible.`
            )
          }
        },
      },
    },
    type: {
      type: DataTypes.STRING,
      comment: 'MIME type of the file that was uploaded',
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    size: {
      type: DataTypes.INTEGER,
      comment: 'Size in bytes of the uploaded file',
      allowNull: false,
      validate: {
        isInt: true,
        min: 1,
      },
    },
    oldFileName: {
      type: DataTypes.STRING,
      comment: 'Filename of the file that was uploaded',
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    hash: {
      type: DataTypes.STRING,
      comment: 'SHA1 Hash of the file',
      allowNull: false,
      validate: {
        isSha1(hash) {
          if (hash.match('[a-fA-F0-9]{40}') === null && hash.length !== 40) {
            throw new Error('Should be a valid sha1 hash.')
          }
        },
      },
    },
  }, {
    comment: 'List of uploaded files',
    instanceMethods: {
      toJSON: defaultToJSON,
    },
    classMethods: {
      associate(models) {
      },
    },
    hooks: {
      afterDestroy(instance) {
        const q = require('q')
        return new q.Promise((resolve, reject, notify) => {
          fs.unlink(instance.location, resolve)
        })
      },
    },
  })

  return File
}

