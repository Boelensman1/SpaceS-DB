const defaultToJSON = require('./util/defaultToJSON')

module.exports = (sequelize, DataTypes) => {
  const Resident = sequelize.define('resident', {
    firstname: {
      type: DataTypes.STRING,
      comment: 'Firstname of the resident',
      allowNull: false,
      validate: {
        len: {
          args: [1, 35],
          msg: 'Should be between 1 and 35 characters.',
        },
      },
    },
    snPrefix: {
      type: DataTypes.STRING,
      comment: 'Surname prefix (tussenvoegsel in dutch) of the resident',
      allowNull: true,
      validate: {
        len: {
          args: [1, 15],
          msg: 'Should be between 1 and 15 characters.',
        },
      },
    },
    surname: {
      type: DataTypes.STRING,
      comment: 'Surname of the resident',
      allowNull: false,
      validate: {
        len: {
          args: [1, 35],
          msg: 'Should be between 1 and 35 characters.',
        },
      },
    },
    email: {
      type: DataTypes.STRING,
      comment: 'Primary email adress of the resident.',
      validate: {
        isEmail: true,
      },
    },
    email2: {
      type: DataTypes.STRING,
      comment: 'Secondary email adress of the resident.',
      allowNull: true,
      validate: {
        isEmail: true,
      },
    },
    phoneNr: {
      type: DataTypes.STRING,
      comment: 'First phone number of the resident, E.164 formatted.',
      allowNull: true,
      validate: {
        is: {
          args: /^\+?[1-9]\d{1,14}$/,
          msg: 'Should be a valid E.164 formatted phone number.',
        },
      },
    },
    phoneNr2: {
      type: DataTypes.STRING,
      comment: 'Second phone number of the resident, E.164 formatted.',
      allowNull: true,
      validate: {
        is: {
          args: /^\+?[1-9]\d{1,14}$/,
          msg: 'Should be a valid E.164 formatted phone number.',
        },
      },
    },
  }, {
    comment: 'Table containing the residents',
    getterMethods: {
      fullName() {
        return `${this.firstname}${this.snPrefix ? (` ${this.snPrefix} `) : ' '}${this.surname}`
      },
    },
    instanceMethods: {
      toJSON: defaultToJSON,
    },
    classMethods: {
      associate(models) {
        Resident.hasOne(models.account, { onDelete: 'CASCADE' })
        Resident.hasOne(models.registerToken, { onDelete: 'CASCADE' })
        Resident.hasOne(models.house)
        Resident.hasOne(models.workUnit)
      },
    },
  })

  return Resident
}

