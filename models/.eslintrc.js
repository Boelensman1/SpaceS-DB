'use strict';

module.exports = {
  'rules': {
    'max-len': [2, 240, 4],
  }
};
