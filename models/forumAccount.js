const defaultToJSON = require('./util/defaultToJSON')

module.exports = (sequelize, DataTypes) => {
  const ForumAccount = sequelize.define('forumAccount', {
    userId: {
      type: DataTypes.INTEGER,
      comment: 'Id of the account on flarum',
      allowNull: false,
      validate: {
        min: 0,
      },
    },
  }, {
    comment: 'The forum account belonging to a resident.',
    instanceMethods: {
      toJSON: defaultToJSON,
    },
    classMethods: {
    },
  })

  return ForumAccount
}

