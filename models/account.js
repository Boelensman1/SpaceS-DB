const _ = require('lodash')
const argon2 = require('argon2')

/**
 * Hook that hashes the password
 *
 * @param {object} instance The instance that the hook runs on
 * @returns {Promise} Promise that resolves when finished
 */
function hashPasswordHook(instance) {
  if (!instance.changed('password')) {
    return
  }
  // eslint-disable-next-line consistent-return
  return argon2.generateSalt().then((salt) => argon2.hash(instance.password, salt)
    .then((hash) => {
      instance.setDataValue('passwordHash', hash)
    })
  )
}

module.exports = (sequelize, DataTypes) => {
  const Account = sequelize.define('account', {
    username: {
      type: DataTypes.STRING,
      comment: 'Username of the account',
      allowNull: false,
      unique: true,
      validate: {
        // below if from http://stackoverflow.com/a/12019115
        // slightly altered (as js does not support lookbehind)
        // but should be the same functionality
        // it validates a valid username
        is: {
          args: ['^(?=.+$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+[^._]$', 'i'],
          msg: 'Does not comply to the username requirements.',
        },
        len: {
          args: [5, 30],
          msg: 'Should be between 5 and 30 characters.',
        },
      },
    },
    password: {
      type: DataTypes.VIRTUAL,
      comment: 'password of the account',
      allowNull: false,
      validate: {
        len: {
          args: [8, 100],
          msg: 'Should be between 8 and 100 characters.',
        },
      },
    },
    passwordHash: {
      type: DataTypes.STRING,
      comment: 'Hashed password of the account',
    },
  }, {
    comment: 'Account used for logging in',
    hooks: {
      beforeCreate: hashPasswordHook,
      beforeUpdate: hashPasswordHook,
    },
    instanceMethods: {
      checkPassword(password) {
        return argon2.verify(this.passwordHash, password)
      },
      toJSON() {
        const excludedAttributes = [
          'password',
          'passwordHash',
        ]
        return _.omit(this.get(), excludedAttributes)
      },
    },
    classMethods: {
      associate(models) {
        Account.belongsTo(models.resident)
        Account.hasOne(models.loginToken, { onDelete: 'CASCADE' })
        Account.belongsTo(models.file, { as: 'profilePicture' })
        Account.hasOne(models.forumAccount, { onDelete: 'CASCADE' })
        Account.hasOne(models.resetPasswordToken, { onDelete: 'CASCADE' })
      },
    },
  })

  return Account
}
