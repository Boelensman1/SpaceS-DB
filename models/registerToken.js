const crypto = require('crypto')
const defaultToJSON = require('./util/defaultToJSON')

module.exports = (sequelize, DataTypes) => {
  const RegisterToken = sequelize.define('registerToken', {
    token: {
      type: DataTypes.STRING,
      comment: 'Token that can be used to register at',
      primaryKey: true,
      allowNull: false,
      unique: true,
    },
  }, {
    comment: 'Table containing the tokens used to register',
    instanceMethods: {
      toJSON: defaultToJSON,
    },
    hooks: {
      beforeValidate(registerToken, options) {
        registerToken.token = crypto.randomBytes(20).toString('hex')
      },
    },
    classMethods: {
      associate(models) {
        RegisterToken.belongsTo(models.resident)
      },
    },
  })

  return RegisterToken
}

