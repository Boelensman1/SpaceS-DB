const defaultToJSON = require('./util/defaultToJSON')

module.exports = (sequelize, DataTypes) => {
  const Workunit = sequelize.define('workUnit', {
    blockNumber: {
      type: DataTypes.INTEGER,
      comment: 'The block the house resides in',
      allowNull: false,
      validate: {
        min: 0,
        max: {
          args: 7,
          msg: 'Should be 7 or lower.',
        },
      },
    },
    floorNr: {
      type: DataTypes.INTEGER,
      comment: 'The number of the floor where the house is located',
      allowNull: false,
      validate: {
        min: 0,
        max: {
          args: 15,
          msg: 'Should be 15 or lower.', // TODO: echte waarde
        },
      },
    },
    constructionWorkUnitNumber: {
      type: DataTypes.INTEGER,
      comment: 'The house number that is used during construction',
      allowNull: false,
      validate: {
        min: 0,
      },
    },
    constructionWorkUnitNumberAddition: {
      type: DataTypes.INTEGER,
      comment: 'Some house\'s with workunits have a addition, for example 5.0.1-2, this notes the -x, 2 in this case',
      validate: {
        min: 0,
      },
    },
    houseNr: {
      type: DataTypes.INTEGER,
      comment: 'The house number that is used on the adress',
      allowNull: false,
      validate: {
        min: 0,
      },
    },
    street: {
      type: DataTypes.STRING,
      comment: 'The street that the house is on',
    },
    zipCode: {
      type: DataTypes.STRING,
      comment: 'The zipcode of the house',
      allowNull: false,
      validate: {
        is: {
          args: /\d{4}[A-Z]{2}/,
          msg: 'Is not a valid zipcode.',
        },
      },
    },
    city: {
      type: DataTypes.STRING,
      comment: 'The city of the house',
      allowNull: false,
    },
  }, {
    comment: 'The houses that are owned by a resident',
    instanceMethods: {
      toJSON: defaultToJSON,
    },
    getterMethods: {
      constructionId() {
        let constructionNumber = `${this.blockNumber}.${this.floorNr
          }.${this.constructionWorkUnitNumber}`
        if (this.constructionWorkUnitNumberAddition) {
          constructionNumber += `-${this.constructionWorkUnitNumberAddition}`
        }
        return constructionNumber
      },
    },
    classMethods: {
      associate(models) {
      },
    },
  })

  return Workunit
}

