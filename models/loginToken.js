const defaultToJSON = require('./util/defaultToJSON')

module.exports = (sequelize, DataTypes) => {
  const LoginToken = sequelize.define('loginToken', {
    token: {
      type: DataTypes.STRING,
      comment: 'The token',
      allowNull: false,
    },
  }, {
    comment: 'The login token of a user',
    instanceMethods: {
      toJSON: defaultToJSON,
    },
    classMethods: {
    },
  })

  return LoginToken
}

