const crypto = require('crypto')
const defaultToJSON = require('./util/defaultToJSON')

module.exports = (sequelize, DataTypes) => {
  const ResetPasswordToken = sequelize.define('resetPasswordToken', {
    token: {
      type: DataTypes.STRING,
      comment: 'Token that can be used to reset password with',
      primaryKey: true,
      allowNull: false,
      unique: true,
    },
  }, {
    comment: 'Table containing the tokens used to register',
    instanceMethods: {
      toJSON: defaultToJSON,
    },
    hooks: {
      beforeValidate(resetPasswordToken, options) {
        resetPasswordToken.token = crypto.randomBytes(20).toString('hex')
      },
    },
    classMethods: {
      associate(models) {
        ResetPasswordToken.belongsTo(models.account)
      },
    },
  })

  return ResetPasswordToken
}

