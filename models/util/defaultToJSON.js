module.exports = function toJSON() {
  // Return a shallow clone so toJSON method of the nested models
  // can be called recursively.
  // see https://github.com/sequelize/sequelize/issues/1462
  return Object.assign({}, this.get())
}
