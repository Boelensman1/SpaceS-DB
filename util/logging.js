const fs = require('fs')
const path = require('path')
const bunyan = require('bunyan')
// eslint-disable-next-line import/no-extraneous-dependencies
const bunyanDebugStream = require('bunyan-debug-stream')

const root = require('app-root-path').toString()
// const prettyStdOut = new PrettyStream();

// prettyStdOut.pipe(process.stdout);

/**
 * Create a bunyan logger
 *
 * @param {object} config The configuration object for defining dir:
 *                        {log directory, level: loglevel}
 * @param {boolean} isAuditLogger Whether or not to create the auditlogger
 * @returns {object} The created logger instance
 */
function createLogger(config, isAuditLogger) {
  const pkg = require('../package') // import ourself
  const appName = pkg.name
  const appVersion = pkg.version
  /* istanbul ignore next */
  const logDir = config.dir || path.join(__dirname, 'logs')
  const logFileName = isAuditLogger ? 'audit' : 'log'
  const logFile = path.join(logDir, `${appName}-${logFileName}.json`)
  const logErrorFile = path.join(logDir, `${appName}-errors.json`)
  /* istanbul ignore next */
  const logLevel = config.level || 'debug'
  const NODE_ENV = process.env.NODE_ENV || 'development'
  const LOG_TO_CONSOLE = process.env.LOG_TO_CONSOLE && true

  // Create log directory if it doesnt exist
  /* istanbul ignore if */
  if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir)
  }


  const streams = [{
    path: logFile,
    level: logLevel,
    type: 'rotating-file',
    period: '1d',
  }]

  if (!isAuditLogger) {
    // push the two other streams
    streams.push({
      path: logErrorFile,
      level: 'error',
    })

    // be more verbose in development
    if (NODE_ENV === 'development' || LOG_TO_CONSOLE) {
      streams.push({
        stream: bunyanDebugStream({
          basepath: root,
          stringifiers: {
            err(err) {
              return JSON.stringify(err, null, 2)
            },
          },
        }),
        level: 'debug',
        type: 'raw',
      })
    } else {
      streams.push({
        stream: bunyanDebugStream({
          basepath: root,
        }),
        level: 'error',
        type: 'raw',
      })
    }
  }

  // Log to console and log file
  const log = bunyan.createLogger({
    name: appName,
    streams,
    serializers: bunyanDebugStream.serializers,
  })

  log.info(`Starting ${appName}, version ${appVersion}`)
  log.info(`Environment set to ${NODE_ENV}`)
  log.debug('Logging setup completed.')

  return log
}

exports.createLogger = createLogger
