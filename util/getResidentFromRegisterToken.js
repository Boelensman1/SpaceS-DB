const models = require('../models')
const q = require('q')

/**
 * Load a resident from a register token
 *
 * @param {string} token The register token that will be used to find
 *                       the resident
 * @param {logger} logger Logger used by restify
 * @param {object} opts Additional options, optional
 *
 * @return {Promise} Promise that resolves into the resident
 */
function getResidentFromRegisterToken(token, logger, opts) {
  return new q.Promise((resolve, reject) => {
    if (opts === undefined) {
      opts = {}
      opts.include = []
    }
    if (!Array.isArray(opts.include)) {
      logger.error('Opts.include must be an array!')
    }
    opts.include.push({
      model: models.registerToken,
      where: {
        token,
      },
    })
    models.resident.find(opts).then((resident) => {
      if (resident) {
        resolve(resident)
      } else {
        reject()
      }
    })
  })
}

module.exports = getResidentFromRegisterToken
