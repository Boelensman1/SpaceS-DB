const models = require('../models')
const getAccountFromLoginToken =
  require('../api/helpers/util/getAccountFromLoginToken')

/**
 * Check a login token
 *
 * @param {req} req from restify
 * @param {string} token The logintoken
 * @param {function} cb Callback, if called with anything other than
 *                   an error it will run the route
 *
 * @return {null} Does not return anything
 */
function checkLoginToken(req, token, cb) {
  const logger = req.log
  const accountId = req.swagger.params.accountId.value

  const err = new Error('Failed to authenticate using login token')
  err.statusCode = 401 // custom error code
  if (!token) {
    cb(err)
    return
  }

  // get the account belonging to this token
  logger.debug('Getting account from logintoken:', token)
  const opts = {}
  opts.include = [
    { model: models.file, as: 'profilePicture' },
    { model: models.forumAccount },
    { model: models.resident },
  ]
  getAccountFromLoginToken(token, logger, opts).then((account) => {
    /* istanbul ignore if */
    logger.debug('done')
    if (account.get('id') !== accountId) {
      cb(err)
      return
    }
    req.account = account
    logger.debug('returing')
    cb()
  }, () => cb(err)
  ).catch((error) => { throw error })
}
module.exports = checkLoginToken
