const models = require('../models')
const getAccountFromResetPasswordToken =
  require('../api/helpers/util/getAccountFromResetPasswordToken')

/**
 * Check a resetPassword token
 *
 * @param {req} req from restify
 * @param {string} token The resetPasswordtoken
 * @param {function} cb Callback, if called with anything other than
 *                   an error it will run the route
 *
 * @return {null} Does not return anything
 */
function checkResetPasswordToken(req, token, cb) {
  const logger = req.log
  const accountId = req.swagger.params.accountId.value

  const err = new Error('Failed to authenticate using resetPassword token')
  err.statusCode = 401 // custom error code
  if (!token) {
    cb(err)
    return
  }

  // get the account belonging to this token
  logger.debug('Getting account from resetPasswordtoken:', token)
  const opts = {}
  opts.include = [
    { model: models.forumAccount },
  ]
  getAccountFromResetPasswordToken(token, logger, opts).then((account) => {
    /* istanbul ignore if */
    logger.debug('done')
    if (account.get('id') !== accountId) {
      cb(err)
      return
    }
    req.account = account
    logger.debug('returing')
    cb()
  }, () => cb(err)
  ).catch((error) => { throw error })
}
module.exports = checkResetPasswordToken
