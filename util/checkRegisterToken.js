const models = require('../models')
const getResidentFromRegisterToken = require('./getResidentFromRegisterToken')

/**
 * Check a login token
 *
 * @param {req} req from restify
 * @param {string} token The logintoken
 * @param {function} cb Callback, if called with anything other than
 *                   an error it will run the route
 *
 * @return {null} Does not return anything
 */
function checkRegisterToken(req, token, cb) {
  const logger = req.log

  const err = new Error('Failed to authenticate using registerToken')
  err.statusCode = 401 // custom error code
  if (!token) {
    cb(err)
    return
  }

  // get the resident belonging to this token
  logger.debug('Getting resident from registerToken:', token)
  const opts = {}
  opts.include = [
    { model: models.house },
  ]
  getResidentFromRegisterToken(token, logger, opts).then((resident) => {
    /* istanbul ignore if */
    req.resident = resident
    cb()
  }, () => cb(err)
  ).catch((error) => { throw error })
}
module.exports = checkRegisterToken
