const q = require('q')
const models = require('../models')

// if process.env.NODE_ENV has not been set, default to development
/* istanbul ignore next */
const NODE_ENV = process.env.NODE_ENV || 'development'

/**
 * Check if a username and password are valid
 *
 * @param {string} username Username of the user
 * @param {string} password password of the user
 * @param {logger} logger Logger used by restify
 *
 * @return {Promise} Promise that resolves into a boolean that is
 *                   True if the username and password are valid
 */
function checkUser(username, password, logger) {
  return new q.Promise((resolve, reject, notify) => {
    // check if we're testing and using the default test username and password
    if (NODE_ENV === 'test' &&
        username === 'testuser' &&
        password === 'testpassword') {
      resolve(true)
      return
    }

    models.admin.findOne({
      where: { userName: username },
    }).then((admin) => {
      if (admin === null) {
        // no admin with that username
        resolve(false)
      } else {
        resolve(admin.checkPassword(password))
      }
    }).catch(
      /* istanbul ignore next */
      (err) => {
        logger.error(err)
        resolve(false)
      }
    )
  })
}

/**
 * Makes sure only authenticated users get access to the api.
 *
 * @param {req} req from restify
 * @param {function} cb Callback, if called with anything other than
 *                   an error it will run the route
 *
 * @return {null} Does not return anything
 */
function basicAuth(req, cb) {
  // res.header('WWW-Authenticate','Basic realm="' + name + '"');

  if (!req.authorization ||
      !req.authorization.basic ||
      !req.authorization.basic.username ||
      !req.authorization.basic.password) {
    const err = new Error('Failed to authenticate using basic auth')
    err.statusCode = 401 // custom error code
    return cb(err)
  }

  const logger = this.log
  const username = req.authorization.basic.username
  const password = req.authorization.basic.password

  return checkUser(username, password, logger).then((valid) => {
    if (!valid) {
      const err = new Error('Failed to authenticate using basic auth')
      err.statusCode = 403 // custom error code
      return cb(err)
    }
    return cb()
  })
}

module.exports = basicAuth
