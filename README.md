# SpaceS DB Server
The database server of the SpaceS community website.

## Prerequisites

### Nodejs and tools

Download and install [Nodejs](https://nodejs.org/en/download/) using your package manager (or under windows by downloading it directly).

#### Mac OS X
To install node under OS X follow [these instructions](http://blog.teamtreehouse.com/install-node-js-npm-mac). You can install postgres using brew as well (*this is recommended!*).

### PostgreSQL

Download and install [PostgreSQL](https://www.postgresql.org/download/). Make sure to also install pgadmin during this installation. Preferably pgadmin3 (and not 4).
During installation use port 5432 and fill in a password of your own choosing.

## Configuration

Rename the sample configuration files in `/config` and adjust the values to your own preference:
 - `default.sample.yaml` to `default.yaml`
 - `test.sample.yaml` to `test.yaml`
 - `production.sample.yaml` to `production.yaml` (if you want to run in production)

There are a few values that you *at least* need to adjust, all of which can be found in `default.yaml`:
- flarum.apiUrl
- flarum.adminUsername
- flarum.adminPassword
- database.password

Run pgadmin and create the same spaces-db username, password and database as defined in `config-db.yaml`:
 - create New Login Role... (right-click Login Roles) with Role name <username> and password <password>
 - create New Database... (right-click Databases) with Name <database> with <username> as owner

## Usage

Go to root of project folder and run from cmd/terminal:
```
  npm install -g yarn
  yarn global add pm2
  yarn global add sequelize-cli
  yarn install
  yarn run dev
```
This should result in a up and running SpaceS database that restarts when you make changes in the source files. You should see  `SpaceS-DB-server listening at http://[::]:8080` in your terminal.

For running in production we use [pm2](https://github.com/Unitech/pm2). You should have it installed globally. Use `yarn start` and `yarn stop` will run pm2 to start and stop the server. `yarn run status` will list out the processes that are running. To monitor use `yarn run monit`. For further options read the documentation of pm2.

## Documentation

To access the documentation for this project, append your host URL with `/docs` (i.e. http://localhost:8080/docs).

## Testing

Run all test specifications with mocha using: `yarn test` or if you want to continually run tests use `yarn run test:watch`.
