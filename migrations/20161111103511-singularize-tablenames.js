module.exports = {
  up(queryInterface, Sequelize) {
    return Sequelize.Promise.all([
      queryInterface.renameTable('accounts', 'account'),
      queryInterface.renameTable('admins', 'admin'),
      queryInterface.renameTable('files', 'file'),
      queryInterface.renameTable('forumAccounts', 'forumAccount'),
      queryInterface.renameTable('houses', 'house'),
      queryInterface.renameTable('registerLinks', 'registerLink'),
      queryInterface.renameTable('residents', 'resident'),
      queryInterface.renameTable('tokens', 'token'),
      queryInterface.renameTable('workUnits', 'workUnit'),
    ])
  },

  down(queryInterface, Sequelize) {
    return Sequelize.Promise.all([
      queryInterface.renameTable('admin', 'admins'),
      queryInterface.renameTable('file', 'files'),
      queryInterface.renameTable('forumAccount', 'forumAccounts'),
      queryInterface.renameTable('house', 'houses'),
      queryInterface.renameTable('registerLink', 'registerLinks'),
      queryInterface.renameTable('resident', 'residents'),
      queryInterface.renameTable('token', 'tokens'),
      queryInterface.renameTable('workUnit', 'workUnits'),
      queryInterface.renameTable('account', 'accounts'),
    ])
  },
}
