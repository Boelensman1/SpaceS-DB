module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable('files',
      {
        id: {
          type: 'INTEGER',
          allowNull: false,
          primaryKey: true,
          autoIncrement: true,
        },
        name: {
          type: 'VARCHAR(255)',
          comment: 'Name of the file',
          allowNull: false,
        },
        location: {
          type: 'VARCHAR(255)',
          comment: 'Location where the file is',
          unique: true,
        },
        type: {
          type: 'VARCHAR(255)',
          comment: 'MIME type of the file that was uploaded',
          allowNull: false,
        },
        size: {
          type: 'INTEGER',
          comment: 'Size in bytes of the uploaded file',
          allowNull: false,
        },
        oldFileName: {
          type: 'VARCHAR(255)',
          comment: 'Filename of the file that was uploaded',
          allowNull: false,
        },
        hash: {
          type: 'VARCHAR(255)',
          comment: 'SHA1 Hash of the file',
          allowNull: false,
        },
        createdAt: {
          type: 'TIMESTAMP WITH TIME ZONE',
          allowNull: false,
        },
        updatedAt: {
          type: 'TIMESTAMP WITH TIME ZONE',
          allowNull: false,
        },
      })
  },
  down(queryInterface, Sequelize) {
    return queryInterface.dropTable('files')
  },
}
