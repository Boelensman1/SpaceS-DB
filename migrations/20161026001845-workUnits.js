module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable('workUnits',
      {
        id: {
          type: 'INTEGER',
          allowNull: false,
          primaryKey: true,
          autoIncrement: true,
        },
        blockNumber: {
          type: 'INTEGER',
          comment: 'The block the house resides in',
          allowNull: false,
        },
        floorNr: {
          type: 'INTEGER',
          comment: 'The number of the floor where the house is located',
          allowNull: false,
        },
        constructionWorkUnitNumber: {
          type: 'INTEGER',
          comment: 'The house number that is used during construction',
          allowNull: false,
        },
        constructionWorkUnitNumberAddition: {
          type: 'INTEGER',
          comment: 'Some house\'s with workunits have a addition, ' +
          'for example 5.0.1-2, this notes the -x, 2 in this case',
        },
        houseNr: {
          type: 'INTEGER',
          comment: 'The house number that is used on the adress',
        },
        createdAt: {
          type: 'TIMESTAMP WITH TIME ZONE',
          allowNull: false,
        },
        updatedAt: {
          type: 'TIMESTAMP WITH TIME ZONE',
          allowNull: false,
        },
        residentId: {
          type: 'INTEGER',
          allowNull: true,
          references: {
            model: 'residents',
            key: 'id',
          },
          onDelete: 'SET NULL',
          onUpdate: 'CASCADE',
        },
      })
  },
  down(queryInterface, Sequelize) {
    return queryInterface.dropTable('workUnits')
  },
}
