module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.renameColumn('account', 'userName', 'username')
  },

  down(queryInterface, Sequelize) {
    return queryInterface.renameColumn('account', 'username', 'userName')
  },
}
