module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable('forumAccounts',
      {
        id: {
          type: 'INTEGER',
          allowNull: false,
          primaryKey: true,
          autoIncrement: true,
        },
        userId: {
          type: 'INTEGER',
          comment: 'Id of the account on flarum',
          allowNull: false,
        },
        createdAt: {
          type: 'TIMESTAMP WITH TIME ZONE',
          allowNull: false,
        },
        updatedAt: {
          type: 'TIMESTAMP WITH TIME ZONE',
          allowNull: false,
        },
        residentId: {
          type: 'INTEGER',
          allowNull: false,
          references: {
            model: 'residents',
            key: 'id',
          },
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
        },
      })
  },
  down(queryInterface, Sequelize) {
    return queryInterface.dropTable('forumAccounts')
  },
}
