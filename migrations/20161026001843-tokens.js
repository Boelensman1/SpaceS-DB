module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable('tokens',
      {
        id: {
          type: 'INTEGER',
          allowNull: false,
          primaryKey: true,
          autoIncrement: true,
        },
        token: {
          type: 'VARCHAR(255)',
          comment: 'The token',
          allowNull: false,
        },
        createdAt: {
          type: 'TIMESTAMP WITH TIME ZONE',
          allowNull: false,
        },
        updatedAt: {
          type: 'TIMESTAMP WITH TIME ZONE',
          allowNull: false,
        },
        residentId: {
          type: 'INTEGER',
          allowNull: false,
          references: {
            model: 'residents',
            key: 'id',
          },
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
        },
      })
  },
  down(queryInterface, Sequelize) {
    return queryInterface.dropTable('tokens')
  },
}
