module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.renameTable('registerLink', 'registerToken')
      .then(() => queryInterface.renameColumn('registerToken', 'link', 'token'))
  },

  down(queryInterface, Sequelize) {
    return queryInterface.renameTable('registerToken', 'registerLink')
      .then(() => queryInterface.renameColumn('registerLink', 'token', 'link'))
  },
}
