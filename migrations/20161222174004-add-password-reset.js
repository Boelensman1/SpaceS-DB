module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable('resetPasswordToken',
      {
        token: {
          type: 'VARCHAR(255)',
          comment: 'Token that can be used to reset password',
          primaryKey: true,
          allowNull: false,
          unique: true,
        },
        createdAt: {
          type: 'TIMESTAMP WITH TIME ZONE',
          allowNull: false,
        },
        updatedAt: {
          type: 'TIMESTAMP WITH TIME ZONE',
          allowNull: false,
        },
        accountId: {
          type: 'INTEGER',
          allowNull: true,
          references: {
            model: 'account',
            key: 'id',
          },
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
        },
      })
  },
  down(queryInterface, Sequelize) {
    return queryInterface.dropTable('resetPasswordToken')
  },
}
