module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable('residents',
      {
        id: {
          type: 'INTEGER',
          allowNull: false,
          primaryKey: true,
          autoIncrement: true,
        },
        firstname: {
          type: 'VARCHAR(255)',
          comment: 'Firstname of the resident',
          allowNull: false,
        },
        snPrefix: {
          type: 'VARCHAR(255)',
          comment: 'Surname prefix (tussenvoegsel in dutch) of the resident',
          allowNull: true,
        },
        surname: {
          type: 'VARCHAR(255)',
          comment: 'Surname of the resident',
          allowNull: false,
        },
        email: {
          type: 'VARCHAR(255)',
          comment: 'Email of the resident.',
          allowNull: false,
        },
        email2: {
          type: 'VARCHAR(255)',
          comment: 'Second Email of the resident.',
          allowNull: true,
        },
        phoneNr: {
          type: 'VARCHAR(255)',
          comment: 'First phone number of the resident, E.164 formatted.',
          allowNull: true,
        },
        phoneNr2: {
          type: 'VARCHAR(255)',
          comment: 'Second phone number of the resident, E.164 formatted.',
          allowNull: true,
        },
        createdAt: {
          type: 'TIMESTAMP WITH TIME ZONE',
          allowNull: false,
        },
        updatedAt: {
          type: 'TIMESTAMP WITH TIME ZONE',
          allowNull: false,
        },
        profilePictureId: {
          type: 'INTEGER',
          allowNull: false,
          references: {
            model: 'files',
            key: 'id',
          },
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
        },
      })
  },
  down(queryInterface, Sequelize) {
    return queryInterface.dropTable('residents')
  },
}
