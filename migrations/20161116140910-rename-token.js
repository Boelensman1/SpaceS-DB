module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.renameTable('token', 'loginToken')
  },

  down(queryInterface, Sequelize) {
    return queryInterface.renameTable('loginToken', 'token')
  },
}
