const fs = require('fs')
const yaml = require('yaml-js')
const findRemoveSync = require('find-remove')

const _ = require('lodash')
const models = require('../../../models')

findRemoveSync(`${__dirname}/models/`, { extensions: ['.yaml'] })

/**
 * Translate from the sequelize typestring to swagger type
 *
 * @param {string} typeString Sequelize typestring
 * @returns {object} Swagger type
 */
function getTypeFromString(typeString) {
  switch (typeString) {
    case 'INTEGER':
      return { type: 'integer' }
    case 'VARCHAR(255)':
      return { type: 'string' }
    case 'TIMESTAMP WITH TIME ZONE':
      return { type: 'string', format: 'date-time' }
    case 'VIRTUAL':
      return false
    default:
      throw new Error(`Did not find a matching type for ${typeString}`)
  }
}

/**
 * Translate from the sequelize type to swagger type
 *
 * @param {object} attrib Sequelize attribute object
 * @returns {object} Swagger type
 */
function getTypeFromAttrib(attrib) {
  const typeString = attrib.type.toString()
  const type = getTypeFromString(typeString)
  if (type && attrib.allowNull) {
    type.type = [type.type, 'null']
  }
  return type
}

/**
 * Translate from the sequelize attribute to swagger description
 *
 * @param {object} attrib Sequelize attribute object
 * @returns {string} Description for swagger
 */
function getDescriptionFromAttrib(attrib) {
  if (attrib.comment) {
    return attrib.comment
  }
  if (attrib.primaryKey === true) {
    return 'The identifier of the instance'
  }
  if (attrib.references) {
    const ref = attrib.references
    return `References the ${ref.key} of the connected ${ref.model}`
  }
  if (attrib.field === 'createdAt') {
    return 'Timestamp when the instance was created'
  }
  if (attrib.field === 'updatedAt') {
    return 'Timestamp when the instance was last updated'
  }
  console.dir(attrib) // eslint-disable-line no-console
  throw new Error('Can\'t find a description for above attrib')
}

/**
 * Parse the sequelize attributes of a object
 *
 * @param {object} attributes List of attributes
 * @returns {object} Parsed arguments
 */
function parseAttributes(attributes) {
  const output = {}
  _.each(attributes, (attribute) => {
    const name = attribute.field
    const type = getTypeFromAttrib(attribute)
    if (!type) { return }
    const description = getDescriptionFromAttrib(attribute)
    output[name] = {}
    if (description) {
      output[name].description = description
    }
    _.merge(output[name], type)
  })
  return output
}

/**
 * Generate a swagger model from a sequelize model
 *
 * @param {object} model Sequelize object
 * @returns {object} object representing the swagger model
 */
function generateSwaggerModel(model) {
  const attributes = parseAttributes(model.attributes)
  return {
    type: 'object',
    properties: attributes,
  }
}

models.init().then(() => {
  _.each(models, (model) => {
    if ({}.hasOwnProperty.call(model, 'attributes')) {
      const modelName = model.name
      const swaggerModel = generateSwaggerModel(model)
      fs.writeFile(`${__dirname}/models/${modelName}.yaml`,
        yaml.dump(swaggerModel),
      (error) => { if (error) { throw error } })
    }
  })
})
