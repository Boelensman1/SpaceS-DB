const models = require('../../models')

const q = require('q')
const handleValidationErrors = require('./util/handleValidationErrors')
const changeAvatarUser = require('flarum-client').changeAvatarUser

/**
 * Set the profile picture
 *
 * @param {object} data The data to put into the profile pic
 * @param {object} logger Logger object, from restify
 * @param {object} res Response object, from restify
 * @param {object} account The account
 * @returns {undefined}
 */
function setProfilePicture(data, logger, res, account) {
  // adapted from stdPost
  const model = models.file
  const url = '/profilePicture/'

  // validate the model
  const profilePic = model.build(data)
  profilePic.validate().then((errors) => {
    if (!errors) {
      // create the profile picture model
      profilePic.save().then(() => {
        // set profilePicture on the account and the forum
        const promises = []
        promises.push(account.setProfilePicture(profilePic))
        if (account.forumAccount) {
          const accountId = account.forumAccount.get('userId')
          promises.push(changeAvatarUser(profilePic, accountId, logger))
        }
        q.all(promises).then(() => {
          const id = profilePic.get('id')
          res.setHeader('Location', url + id)
          res.status(201).end()
        })
      })
      .catch(
        /* istanbul ignore next */
        (err) => {
          if (err.name === 'SequelizeUniqueConstraintError') {
            return handleValidationErrors(err, res, logger)
          }
          throw err
        }
      )
    } else {
      handleValidationErrors(errors, res, logger)
    }
  })
  .catch(
    /* istanbul ignore next */
    (err) => {
      throw err
    }
  )
}

module.exports = setProfilePicture
