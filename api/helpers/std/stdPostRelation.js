const getIndex = require('../util/getIndex')
const getName = require('../util/getName')

/**
 * Add a object and relate it to its parent
 *
 * @param {object} model The model to set the relation on
 * @param {int|string} id The identifier of the instance to set the relation on
 * @param {object} modelRelation The model of the relation
 * @param {object} body The values of the relation
 * @param {object} req Request object, from restify
 * @param {object} res Response object, from restify
 * @param {function} next Function to call when we're done, from restify
 * @returns {undefined}
 */
function stdPostRelation(model, id, modelRelation, body, req, res, next) {
  return model.findById(id).then((result) => {
    // check if we found the instance
    if (result == null) {
      // send the error
      res.status(404).send(`Item with id (${id}) not found.`)
    } else {
      modelRelation.create(body).then((created) => {
        const url = `/${getName(created)}/`
        res.setHeader('Location', url + getIndex(created))
        res.status(201).send(created.toJSON())
      })
    }
  }).catch(
    /* istanbul ignore next */
    (err) => {
      throw err
    }
  )
}

module.exports = stdPostRelation
