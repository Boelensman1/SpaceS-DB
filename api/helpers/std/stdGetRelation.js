/**
 * Get one specific instance
 *
 * @param {object} model The model belonging to this route
 * @param {int|string} id The identifier of the instance
 * @param {string} relation Name of the relation
 * @param {object} req Request object, from restify
 * @param {object} res Response object, from restify
 * @returns {undefined}
 */
function stdGetRelation(model, id, relation, req, res) {
  const opts = {}
  opts.include = relation.model
  return model.findById(id, opts).then((result) => {
    // check if we found the instance
    if (result != null) {
      const relationResult = result.get(relation.name)
      if (relationResult) {
        res.status(200).send(relationResult)
      } else {
        res.status(404).send(`Item does not have a ${relation.name} attached.`)
      }
    } else {
      // send the error
      res.status(404).send(`Item with id (${id}) not found.`)
    }
  }).catch(
    /* istanbul ignore next */
    (err) => {
      throw err
    }
  )
}

module.exports = stdGetRelation
