const getIndex = require('../util/getIndex')
const getName = require('../util/getName')
const handleValidationErrors = require('../util/handleValidationErrors')

/**
 * Add a object
 *
 * @param {object} model The model to set the relation on
 * @param {object} body The values of the relation
 * @param {object} req Request object, from restify
 * @param {object} res Response object, from restify
 * @returns {undefined}
 */
function stdPost(model, body, req, res) {
  model.create(body).then((created) => {
    const url = `/${getName(created)}/`
    res.setHeader('Location', url + getIndex(created))
    res.status(201).send(created.toJSON())
  }).catch((err) => {
    handleValidationErrors(err, res, req.log)
  })
}

module.exports = stdPost
