const settings = require('config')
const models = require('../../models')

// for mail
const path = require('path')
const nodemailer = require('nodemailer')

// create reusable transporter object using the default SMTP transport
const transporter = nodemailer.createTransport(settings.smtp.url)
const mailDefaultOptions = settings.smtp.options

const EmailTemplate = require('email-templates').EmailTemplate

const root = require('app-root-path').toString()

const template = path.join(root, 'mails', 'reset-password-mail')
const resetPasswordMail = new EmailTemplate(template)

const getIndex = require('./util/getIndex')

/**
 * Send a reset password mail to an account
 *
 * @param {object} resident The resident to who we send the password forgot link
 * @param {object} logger The logger instance
 * @returns {Promise} Promise that resolves into the created token
 */
function sendResetPasswordMail(resident, logger) {
  const Promise = models.Sequelize.Promise

  const account = resident.get('account')
  return models.resetPasswordToken.create({
    accountId: account.id,
  }).then((created) => (
    new Promise((resolve, reject) => {
      const email = resident.get('email')

      const clientUrls = settings.client
      const baseResetPassUrl = clientUrls.baseUrl + clientUrls.resetPasswordUrl
      const resetPasswordLink = `${baseResetPassUrl}/${getIndex(created)}`

      const replace = { resetPasswordLink, baseUrl: settings.server.host }
      resetPasswordMail.render(replace, (err, result) => {
        const mailOptions = Object.assign(mailDefaultOptions, {
          to: email,
          subject: result.subject,
          text: result.text,
          html: result.html,
        })

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            logger.error(error)
            reject(error)
          } else {
            logger.info('Message sent:', info.response)
            resolve(created)
          }
        })

        // while sending remove all other reset tokens of this user
        models.resetPasswordToken.destroy({
          where: {
            token: { $ne: created.token }, // token != created.token
            accountId: created.accountId,
          },
        })
      })
    })
  ))
}

module.exports = sendResetPasswordMail
