

const messages = {
  'Validation isBoolean failed': 'Should be either true or false.',
  'Validation isDate failed': 'Should be a valid date.',
  'Validation min failed': 'Should be a positive number.',
  'Validation notEmpty failed': 'Should not be empty.',
  'Validation isInt failed': 'Should be a number.',
  'Validation isNumeric failed': 'Should be a number.',
  'Validation isEmail failed': 'Should be a valid email adress.',
}

/**
 * Format the output of a failed sequelize validate
 *
 * @param {string} message Input message, from sequelize
 * @returns {string} The formatted message
 */
function validatorFormatter(message) {
  if ({}.hasOwnProperty.call(messages, message)) {
    return messages[message]
  }

  const replaceBy = 'Is not allowed to be empty.'
  if (message.endsWith('cannot be null')) {
    message = replaceBy
  }

  // return the formatted message
  return message
}

module.exports = validatorFormatter
