/**
 * Handles the formatting of flarum errors
 *
 * @param {array} errors The errors, from sequelizejs
 * @param {object} res the response, from restify
 * @param {object} logger Logger object, from restify
 * @returns {undefined}
 */
function handleFlarumErrors(errors, res, logger) {
  const errorlist = {}
  errors.errors.forEach((error) => {
    const path = error.source.pointer.replace('/data/attributes/', '')
    errorlist[path] = error.detail
  })

  if (process.env.NODE_ENV !== 'test') {
    logger.error(errorlist)
  }
  res.status(400).send(errorlist)
}

module.exports = handleFlarumErrors
