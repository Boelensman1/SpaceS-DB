/**
 * Get the index of a created instance
 *
 * @param {object} instance The created instance
 * @returns {string} The index
 */
function getIndex(instance) {
  const uniqueKeys = instance.$modelOptions.uniqueKeys
  const key = uniqueKeys[Object.keys(uniqueKeys)[0]]
  if (!key) { return instance.get('id') }
  return instance.get(key.column)
}

module.exports = getIndex
