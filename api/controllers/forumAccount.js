

const models = require('../../models')
const stdGetAll = require('../helpers/std/stdGetAll')
const stdGetSpecific = require('../helpers/std/stdGetSpecific')

/**
 * Get all forum accounts
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getForumAccountList(req, res) {
  return stdGetAll(models.forumAccount, req, res)
}

/**
 * Get a specific forum account
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getForumAccount(req, res) {
  const id = req.swagger.params.id.value
  return stdGetSpecific(models.forumAccount, id, req, res)
}

module.exports = {
  getForumAccountList,
  getForumAccount,
}

