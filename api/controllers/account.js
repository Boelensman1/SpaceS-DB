const settings = require('config')

const models = require('../../models')
const handleFlarumErrors = require('../helpers/util/handleFlarumErrors')
const handleValidationErrors =
  require('../helpers/util/handleValidationErrors')

const stdGetAll = require('../helpers/std/stdGetAll')
const stdGetSpecific = require('../helpers/std/stdGetSpecific')

// for profilepicture
const mime = require('mime')
const fs = require('fs')

const setProfilePicture = require('../helpers/setProfilePicture')
const getFileName = require('../helpers/getFileName')
const sha1 = require('node-sha1')

const flarumClient = require('flarum-client')

const sendResetPasswordMail = require('../helpers/sendResetPasswordMail')


/**
 * Get all accounts
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getAccountList(req, res) {
  return stdGetAll(models.account, req, res)
}


/**
 * Get a specific account
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getAccount(req, res) {
  const id = req.swagger.params.id.value
  return stdGetSpecific(models.account, id, req, res)
}

/**
 * Send reset password mail from a username
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function sendResetPasswordMailByUsername(req, res) {
  const logger = req.log
  const username = req.swagger.params.username.value

  // find the account
  const opts = {
    include: {
      model: models.account,
      where: {
        username,
      },
    },
  }
  models.resident.findOne(opts).then((resident) => {
    // check if we found the instance
    if (resident == null) {
      // send the error
      return res.status(404).send(`Account with name ${username} not found.`)
    }
    return sendResetPasswordMail(resident, logger).then((created) => (
      res.status(201).send(created.toJSON())
    ))
  }).catch(
    /* istanbul ignore next */
    (err) => {
      throw err
    }
  )
}

/**
 * Send reset password mail from an email
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function sendResetPasswordMailByEmail(req, res) {
  const logger = req.log
  const email = req.swagger.params.email.value

  // find the account
  const opts = {
    where: {
      email,
    },
    include: {
      model: models.account,
      required: true,
    },
  }
  models.resident.findOne(opts).then((resident) => {
    // check if we found the instance
    if (resident == null) {
      // send the error
      return res.status(404).send(`Account with email ${email} not found.`)
    }
    return sendResetPasswordMail(resident, logger).then((created) => (
      res.status(201).send(created.toJSON())
    ))
  }).catch(
    /* istanbul ignore next */
    (err) => {
      throw err
    }
  )
}

/**
 * Reset password of account
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function resetPassword(req, res) {
  const logger = req.log
  const { password } = req.swagger.params.body.value
  const account = req.account // from security
  const forumId = account.forumAccount.userId

  const previousHash = account.passwordHash

  return account.update({ password }).then(() => (
    flarumClient.patchUser({ password }, forumId, logger)
    .then(() => (
      // destroy the resetpassword token and send the OK
      account.resetPasswordToken.destroy().then(res.status(200).end())
    )).catch((err) => (
      account.update({ passwordHash: previousHash }).then(() => (
        // the password was not changed
        handleFlarumErrors(err, res, logger)
      ))))
  )).catch((err) => handleValidationErrors(err, res, logger))
}


/**
 * Delete a account
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function deleteAccount(req, res) {
  const id = req.swagger.params.id.value
  const Promise = models.Sequelize.Promise

  const opts = { include: models.forumAccount }
  models.account.findById(id, opts).then((account) => {
    if (!account) {
      return res.status(404).end()
    }
    const forumAccountId = account.forumAccount.get('userId')
    return Promise.join(
      account.destroy(),
      flarumClient.deleteUser(forumAccountId),
      () => {
        res.status(200).end()
      })
  })
}

/**
 * Add an account to a resident
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function addAccount(req, res) {
  const logger = req.log
  const body = req.swagger.params.body.value
  const username = body.username
  const password = body.password

  const resident = req.resident // from authentication

  const registerToken = req.resident.get('registerToken')
  const token = registerToken.get('token')
  logger.debug('Resident trying to create account with token', token)
  // check if we found the instance
  const account = models.account.build({
    username,
    password,
    residentId: resident.id,
  })
  account.validate().then((error) => {
    if (error) { return handleValidationErrors(error, res, logger) }
    return account.save().then(() => {
      const email = resident.get('email')
      // try and register an account on the forum
      return flarumClient.createUser(
        username,
        password,
        email,
        resident,
        logger
      ).then((result) => {
        // everything OK!
        // lets create the account for this user
        logger.debug('Creating account')
        // lets link this user in the database
        logger.debug('Tokening user in the database')
        const userId = result.id
        models.forumAccount.create({
          userId,
        }).then((forumAccount) => {
          account.setForumAccount(forumAccount).then(() => {
            // delete the register token
            logger.info('Account succesfully created using', token)
            const url = '/account/'
            res.setHeader('Location', url + account.get('id'))
            res.status(201).send(account.toJSON())

            if (settings.destroyRegisterTokenAfterUse) {
              registerToken.destroy()
            }
          })
        })
      }).catch((err) => {
        // the account was not created, we will have to remove it
        account.destroy().then(() =>
          // format the errors and send
          handleFlarumErrors(err, res, logger))
      })
    }).catch((err) => {
      if (err.name === 'SequelizeUniqueConstraintError') {
        return handleValidationErrors(err, res, logger)
      }
      throw err
    })
  }).catch((err) => { handleValidationErrors(err, res, logger) })
}

/**
 * Path a forum account
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function patchForumAccount(req, res) {
  // const model = models.work;
  const logger = req.log
  const body = req.swagger.params.body.value

  // this comes from the security check
  const account = req.account

  const forumId = account.forumAccount.userId
  return flarumClient.patchUser(body, forumId, logger)
    .then((result) => {
      res.status(200).send(account.forumAccount.toJSON())
    })
}


/**
 * Add a profile picture
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function addProfilePicture(req, res) {
  const logger = req.log

  // this comes from the security check
  const account = req.account

  const file = req.swagger.params.file.value

  // set the properties that come from the uploaded file
  const data = {}
  data.name = `Profile icon ${account.resident.get('fullName')}`
  data.oldFileName = file.originalname
  data.type = file.mimetype
  data.size = file.size

  const extention = mime.extension(file.mimetype)

  logger.debug('Received file: ', data)

  // check if filename exists
  const fileLocation = getFileName(
    settings.profileIconLocation,
    extention)

  // move the file
  logger.debug('Moving file to', fileLocation)
  const dest = fs.createWriteStream(fileLocation)
  dest.write(file.buffer)
  dest.end()

  // calculate hash of file while moving
  data.hash = sha1(file.buffer)

  dest.on('finish', () => {
    data.location = fileLocation
    // remove old file
    if (account.get('profilePicture')) {
      account.get('profilePicture').destroy().then(() => {
        setProfilePicture(data, logger, res, account)
      })
    } else {
      setProfilePicture(data, logger, res, account)
    }
  })
  /* istanbul ignore next */
  dest.on('error', (err) => {
    throw err
  })
}

/**
 * Delete a login token
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function deleteLoginToken(req, res) {
  req.account.loginToken.destroy().then(() => (
    res.status(200).end()
  ))
}


module.exports = {
  getAccountList,
  getAccount,
  deleteAccount,
  addAccount,
  resetPassword,
  sendResetPasswordMailByUsername,
  sendResetPasswordMailByEmail,
  patchForumAccount,
  addProfilePicture,
  deleteLoginToken,
}
