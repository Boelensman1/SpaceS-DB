const flarumClient = require('flarum-client')
const stdGetAll = require('../helpers/std/stdGetAll')
const models = require('../../models')


/**
 * Get all login tokens
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getLoginTokenList(req, res) {
  return stdGetAll(models.loginToken, req, res)
}

/**
 * Add a login token to a account
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function addLoginToken(req, res) {
  const logger = req.log
  const body = req.swagger.params.body.value
  const username = body.username
  const password = body.password

  logger.info('Getting user logintoken for', username)
  return flarumClient.getUserToken(username, password, logger)
    .then((result) => {
      const tokenString = result.token

      if (username.toLowerCase() === 'admin') {
        // TODO: replace this with a admin interface
        // its the admin, lets let him through
        models.loginToken.create({ token: tokenString }).then((token) => {
          const url = '/loginToken/'
          res.setHeader('Location', url + token.get('id'))
          res.status(201).send(token.toJSON())
        })
        return
      }
      const userId = result.userId
      // Lets load the resident
      models.resident.findOne({
        include: {
          model: models.account,
          required: true,
          include: {
            model: models.forumAccount,
            required: true,
            where: { userId },
          },
        },
      }).then((resident) => {
        // check if we found a resident
        if (!resident) {
          logger.error('Getting of user token for', username, 'was denied',
          'because of invalid userid')
          res.status(403).end()
          return
        }

        models.loginToken.create({ token: tokenString }).then((token) => {
          resident.account.setLoginToken(token).then(() => {
            const url = '/loginToken/'
            res.setHeader('Location', url + token.get('id'))
            res.status(201).send(token.toJSON())
          })
        })
      })
    }).catch((errors) => {
      res.status(401).send({ error: 'Username or password incorrect' })
    })
}

module.exports = {
  getLoginTokenList,
  addLoginToken,
}
