const fs = require('fs')
const models = require('../../models')
const getProfilePicById = require('../helpers/getProfilePicById')

/**
 * Get all houses
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getProfilePictureList(req, res) {
  // get all files that are connected to an account as a profilePicture
  return models.account.findAll({
    attributes: ['id'],
    include: [
      { model: models.file, as: 'profilePicture', required: true },
    ],
  }).then((result) => {
    res.status(200).send(result.map((value) => (value.profilePicture)))
  }).catch(
    /* istanbul ignore next */
    (err) => {
      throw err
    }
  )
}

/**
 * Get a specific profile picture
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getProfilePicture(req, res) {
  const id = req.swagger.params.id.value
  getProfilePicById(id).then((result) => {
    if (result != null) {
      res.status(200).send(result)
    } else {
      // send the error
      res.status(404).send(`Item with id (${id}) not found.`)
    }
  }).catch(
    /* istanbul ignore next */
    (err) => {
      throw err
    }
  )
}

/**
 * View a profile picture
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function viewProfilePicture(req, res) {
  const id = req.swagger.params.id.value
  getProfilePicById(id).then((result) => {
    if (result != null) {
      fs.readFile(result.location, (err, file) => {
        if (err) {
          res.writeHead(500)
          res.end()
        } else {
          res.writeHead(200)
          res.write(file)
          res.end()
        }
      })
    } else {
      // send the error
      res.status(404).send(`Item with id (${id}) not found.`)
    }
  }).catch(
    /* istanbul ignore next */
    (err) => {
      throw err
    }
  )
}

module.exports = {
  getProfilePictureList,
  getProfilePicture,
  viewProfilePicture,
}

