const models = require('../../models')
const stdGetAll = require('../helpers/std/stdGetAll')
const stdGetSpecific = require('../helpers/std/stdGetSpecific')

/**
 * Get all residents
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getResetPasswordTokenList(req, res) {
  return stdGetAll(models.resetPasswordToken, req, res)
}

/**
 * Get a specific resident
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getResetPasswordToken(req, res) {
  const id = req.swagger.params.id.value
  return stdGetSpecific(models.resetPasswordToken, id, req, res)
}

module.exports = {
  getResetPasswordTokenList,
  getResetPasswordToken,
}

