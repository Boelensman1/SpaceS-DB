const fs = require('fs')
const root = require('app-root-path').toString()

/**
 * Serve  static files
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function serveStatic(req, res) {
  const query = req.url.replace('/static/', '')
  const path = `${root}/static/${query}`
  fs.access(path, fs.constants.R_OK, (error) => {
    if (error) {
      res.status(404).send(`Cannot GET /${query}`)
    } else {
      res.sendfile(`${root}/static/${query}`, (err) => {
        if (err) {
          req.log.error(err)
          res.status(err.status).end()
        }
      })
    }
  })
}

module.exports = {
  serveStatic,
}
