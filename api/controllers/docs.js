const root = require('app-root-path').toString()

/**
 * Serve docs
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function serveDocs(req, res) {
  res.sendfile(`${root}/docs/index.html`, (err) => {
    if (err) {
      req.log.error(err)
      res.status(err.status).end()
    }
  })
}

module.exports = {
  serveDocs,
}
