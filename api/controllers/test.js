/**
 * Simple test route
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function test(req, res) {
  res.status(200).send({ result: 'OK' })
}

/**
 * Simple test route with auth
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function testWithAuth(req, res) {
  res.status(200).send({ result: 'OK-AUTH' })
}

module.exports = {
  test,
  testWithAuth,
}
