

const models = require('../../models')
const stdGetAll = require('../helpers/std/stdGetAll')
const stdGetSpecific = require('../helpers/std/stdGetSpecific')

/**
 * Get all work units
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getWorkUnitList(req, res) {
  return stdGetAll(models.workUnit, req, res)
}

/**
 * Get a specific work unit
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getWorkUnit(req, res) {
  const id = req.swagger.params.id.value
  return stdGetSpecific(models.workUnit, id, req, res)
}


module.exports = {
  getWorkUnitList,
  getWorkUnit,
}

