const SwaggerMW = require('swagger-express-mw')

const authenticate = require('./util/authenticate')
const checkLoginToken = require('./util/checkLoginToken.js')
const checkRegisterToken = require('./util/checkRegisterToken.js')
const checkResetPasswordToken = require('./util/checkResetPasswordToken.js')

const settings = require('config')

// console.log(settings)

const config = {
  appRoot: __dirname, // required config
  swaggerSecurityHandlers: {
    admin(req, authOrSecDef, scopesOrApiKey, cb) {
      return authenticate(req, cb)
    },
    loginToken(req, authOrSecDef, scopesOrApiKey, cb) {
      return checkLoginToken(req, scopesOrApiKey, cb)
    },
    registerToken(req, authOrSecDef, scopesOrApiKey, cb) {
      return checkRegisterToken(req, scopesOrApiKey, cb)
    },
    resetPasswordToken(req, authOrSecDef, scopesOrApiKey, cb) {
      return checkResetPasswordToken(req, scopesOrApiKey, cb)
    },
  },
}

const worker = require('./worker')
const logging = require('./util/logging')
const models = require('./models')

// if process.env.NODE_ENV has not been set, default to development
/* istanbul ignore next */
const NODE_ENV = process.env.NODE_ENV || 'development'

/**
 * Spawn a worker that handles requests
 *
 * @param {object} swaggerExpress The swagger object
 * @param {object} logger The logger object
 * @param {object} auditLogger Instance of the auditLogger
 * @param {string} address Optional, the address to listen on
 * @param {number} port Optional, the port to listen on
 * @returns {undefined}
 */
function spawnWorker(swaggerExpress, logger, auditLogger, address, port) {
  // create servers
  worker.createServer(swaggerExpress, logger, auditLogger, (app) => {
    /* istanbul ignore else */
    if (port === undefined) {
      /* istanbul ignore next*/
      port = settings.server.port || 8000
    }
    /* istanbul ignore else */
    if (address === undefined) {
      address = settings.server.address || null
    }

    // set title
    app.locals.title = settings.name

    // start listening
    const server = app.listen(port, address, () => {
      logger.info('%s listening at %s:%s',
        app.locals.title,
        server.address().address,
        server.address().port)
    })
  })
}

/**
 * Start the server
 *
 * @param {string} address Optional, the address to listen on
 * @param {number} port Optional, the port to listen on
 * @returns {undefined}
 */
function run(address, port) {
  SwaggerMW.create(config, (err, swaggerExpress) => {
    if (err) { throw err }

    // Set up logging
    const logger = logging.createLogger(settings.logs)
    const auditLogger = logging.createLogger(settings.logs, true)

    // initialize the flarum client
    require('flarum-client').init(settings.flarum, logger)

    // sync the models
    models.init(logger).then(() => {
      spawnWorker(swaggerExpress, logger, auditLogger, address, port)
    })

    if (NODE_ENV === 'development') {
      // install response validation listener
      swaggerExpress.runner.on('responseValidationError',
        (validationResponse, request, response) => {
          // log your validationResponse here...
          logger.error({ err: validationResponse.errors }, 'Validation failed')
        }
      )
    }
  })
}

exports.run = run
