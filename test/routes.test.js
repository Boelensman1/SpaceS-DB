const request = require('supertest')

// comparison lib
const chai = require('chai')
chai.use(require('chai-datetime'))
chai.use(require('chai-things'))

const models = require('../models')
const data = require('./data')


chai.config.includeStack = true
const expect = chai.expect

const settings = require('config')

const only = settings.testOnlyRoute

require('flarum-client').init(settings.flarum)


const routesTest = require('./routesTest')

const url = `http://localhost:${settings.server.port}`

/**
 * Capitalize the first letter of a string
 *
 * @param {string} string The string to be capitalized
 * @returns {string} The input string with its first letter capitalized
 */
function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

describe('Test the routes defined in ./routesTest', function testRoutes() {
  this.timeout(5000)
  before((done) => {
    require('../index')

    // make sure the server is started
    setTimeout(() => {
      request(url)
      .get('/test')
      .set('Accept', 'application/json')
      .end((err) => {
        if (err) {
          if (err.code === 'ECONNREFUSED') {
            return done(new Error('Server is not running.'))
          }
          return done(err)
        }
        return done()
      })
    }, 2500)
  })

  // ok, now lets test all the routes
  routesTest.forEach((element) => {
    if (only && element.name === only) {
      describe.only(capitalizeFirstLetter(element.name), () => {
        element.test(url, data, models, expect)
      })
    }
    describe(capitalizeFirstLetter(element.name), () => {
      element.test(url, data, models, expect)
    })
  })
})
