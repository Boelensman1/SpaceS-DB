// comparison lib
const chai = require('chai')
chai.use(require('chai-datetime'))
chai.use(require('chai-things'))
chai.use(require('chai-as-promised'))

const expect = chai.expect
chai.config.includeStack = true

const models = require('../../models')

describe('Test the forumAccount model', () => {
  it('Should set relation with account', () => (
    models.init().then(() => (
      models.forumAccount.create({ userId: 0 }).then((forumAccount) => (
        models.account.create({
          username: 'TestUname',
          password: 'TestPassword',
        }).then((account) => (
          account.setForumAccount(forumAccount).then(() => (
            account.getForumAccount().then((forumAccRel) => {
              expect(forumAccRel.toJSON()).to.eql(forumAccount.get())
            })
          ))
        ))
      ))
    ))
  ))
})
