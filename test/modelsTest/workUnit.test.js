const chai = require('chai')
chai.use(require('chai-string'))
chai.use(require('chai-as-promised'))

const expect = chai.expect
chai.config.includeStack = true

const models = require('../../models')

describe('Test the workUnit model', () => {
  it('Should correctly get the constructionId', () => models.init().then(() => {
    let workUnit = models.workUnit.build({
      blockNumber: 1,
      floorNr: 5,
      constructionWorkUnitNumber: 4,
      houseNr: 2,
    })
    expect(workUnit.constructionId).to.equal('1.5.4')

      // now without roomNr
    workUnit = models.workUnit.build({
      blockNumber: 2,
      floorNr: 1,
      constructionWorkUnitNumber: 5,
      constructionWorkUnitNumberAddition: 2,
      houseNr: 2,
    })
    expect(workUnit.constructionId).to.equal('2.1.5-2')
  }))
})
