const chai = require('chai')
chai.use(require('chai-string'))
chai.use(require('chai-as-promised'))

const expect = chai.expect
chai.config.includeStack = true

const models = require('../../models')

describe('Test the house model', () => {
  it('Should correctly get the constructionId', () => models.init().then(() => {
    let house = models.house.build({
      blockNumber: 1,
      floorNr: 5,
      constructionHouseNumber: 4,
      roomNr: 3,
      houseNr: 2,
    })
    expect(house.constructionId).to.equal('1.5.4.3')

      // now without roomNr
    house = models.house.build({
      blockNumber: 2,
      floorNr: 1,
      constructionHouseNumber: 5,
      roomNr: null,
      houseNr: 2,
    })
    expect(house.constructionId).to.equal('2.1.5')

      // now without roomNr
    house = models.house.build({
      blockNumber: 2,
      floorNr: 1,
      constructionHouseNumber: 5,
      constructionHouseNumberAddition: 2,
      roomNr: 6,
      houseNr: 2,
    })
    expect(house.constructionId).to.equal('2.1.5.6-2')
  }))
})
