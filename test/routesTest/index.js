const fs = require('fs')
const path = require('path')


/**
 * Add a testfile to the stack of tests to run
 *
 * @param {string} file The name of the file
 * @param {array} dataList The list to add the file to
 * @returns {undefined}
 */
function importTestRoute(file, dataList) {
  const name = path.basename(file, '.js')
  dataList.push({
    name,
    test: require(`./${name}`), // eslint-disable-line import/no-dynamic-require
  })
}

const routesTest = []
const testRoutes = fs.readdirSync(__dirname).filter((file) => (
  (file.indexOf('.') !== 0) && (file !== 'index.js')
))

// import the test routes
testRoutes.forEach((testRoute) => (importTestRoute(testRoute, routesTest)))

module.exports = routesTest
