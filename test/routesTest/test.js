const request = require('supertest')

module.exports = function testTestRoute(url, data, models, expect) {
  it('should return the correct test route (route:  /test)', (done) => {
    request(url)
    .get('/test')
    .set('Accept', 'application/json')
    .expect('Content-Type', 'application/json; charset=utf-8')
    .expect(200)
    .end((err, res) => {
      if (err) {
        return done(err)
      }
      const resp = res.body
      expect(resp).to.be.an('object')
      expect(resp.result).to.be.equal('OK')
      return done()
    })
  })
}
