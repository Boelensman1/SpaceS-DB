const q = require('q')
const request = require('supertest')

const insertExampleNoPost = require('../util/insertExampleNoPost')
const insertMultiple = require('../util/insertMultiple')
const insertExample = require('../util/insertExample')
const checkExample = require('../util/checkExample')
const checkMultiple = require('../util/checkMultiple')
const checkBadExampleNoPost = require('../util/checkBadExampleNoPost')
const prepareTestData = require('../util/prepareTestData')
const createResidentAndRegisterToken =
  require('../util/createResidentAndRegisterToken')

const nameOfRoute = 'resident'

/**
 * Test getting a single object
 *
 * @param {string} url Url of the server
 * @param {object} data Testdata of the model to which the route gives access
 * @param {object} models The sequelize models
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testGetOne(url, data, models, expect) {
  const testData = prepareTestData(
    data[nameOfRoute].examples[0],
    data[nameOfRoute]
  )

  return insertExampleNoPost(
    models[nameOfRoute],
    testData
  ).then((id) => (
    checkExample(
      id,
      models[nameOfRoute],
      url,
      `/${nameOfRoute}`,
      data[nameOfRoute],
      testData,
      expect
    )
  ))
}

/**
 * Test posting a single object
 *
 * @param {string} url Url of the server
 * @param {object} data Testdata of the model to which the route gives access
 * @param {object} models The sequelize models
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testAdd(url, data, models, expect) {
  const testData = prepareTestData(
    data[nameOfRoute].examples[0],
    data[nameOfRoute]
  )

  const locationExpect = new RegExp(`/${nameOfRoute}/(\\d)*`)

  return insertExample(
    testData,
    url,
    `/${nameOfRoute}/`,
    locationExpect,
    expect
  ).then((id) => (
    checkExample(
      id,
      models[nameOfRoute],
      url,
      `/${nameOfRoute}`,
      data[nameOfRoute],
      testData,
      expect
    )
  ))
}

/**
 * Test patching a object
 *
 * @param {string} url Url of the server
 * @param {object} data Testdata of the model to which the route gives access
 * @param {object} models The sequelize models
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testPatch(url, data, models, expect) {
  const testData = prepareTestData(
    data[nameOfRoute].examples[0],
    data[nameOfRoute]
  )
  const testData2 = prepareTestData(
    data[nameOfRoute].examples[1],
    data[nameOfRoute]
  )

  return insertExampleNoPost(
    models[nameOfRoute],
    testData
  ).then((id) => (
    new q.Promise((resolve, reject) => {
      request(url)
        .patch(`/${nameOfRoute}/${id}`)
        .send(testData2)
        .set('Accept', 'application/json')
        .auth('testuser', 'testpassword')
        .expect(200)
        .end((err, res) => {
          if (err) {
            reject(err)
          }
          return resolve(checkExample(
            id,
            models[nameOfRoute],
            url,
            `/${nameOfRoute}`,
            data[nameOfRoute],
            testData2,
            expect
          ))
        })
    })
  ))
}

/**
 * Test getting a registerToken via the resident route
 *
 * @param {string} url Url of the server
 * @param {object} data Testdata of the model to which the route gives access
 * @param {object} models The sequelize models
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testGetRegisterTokenRelation(url, data, models, expect) {
  return createResidentAndRegisterToken(models).then((result) => (
    checkExample(
      result.registerToken.get('token'),
      models.registerToken,
      url,
      `/${nameOfRoute}/${result.resident.get('id')}/registerToken`,
      {},
      result.registerToken.toJSON(),
      expect,
      true,
      true
    )
  ))
}

/**
 * Test adding a registerToken via the resident route
 *
 * @param {string} url Url of the server
 * @param {object} data Testdata of the model to which the route gives access
 * @param {object} models The sequelize models
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testAddRegisterTokenRelation(url, data, models, expect) {
  const testData = prepareTestData(
    data[nameOfRoute].examples[0],
    data[nameOfRoute]
  )

  const testDataToken = prepareTestData(
    data.registerToken.examples[0],
    data.registerToken
  )

  const locationExpect = /\/registerToken\/.+/

  return insertExampleNoPost(
    models[nameOfRoute],
    testData
  ).then((residentId) => (
    insertExample(
      testDataToken,
      url,
      `/${nameOfRoute}/${residentId}/registerToken`,
      locationExpect,
      expect
    ).then((id) => (
      checkExample(
        id,
        models.registerToken,
        url,
        `/${nameOfRoute}`,
        data.registerToken,
        testDataToken,
        expect,
        false
      )
    ))
  ))
}

/**
 * Test getting a house via the resident route
 *
 * @param {string} url Url of the server
 * @param {object} data Testdata of the model to which the route gives access
 * @param {object} models The sequelize models
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testGetRegisterHouseRelation(url, data, models, expect) {
  const testData = prepareTestData(
    data.house.examples[0],
    data.house
  )

  return createResidentAndRegisterToken(models).then((result) => (
    models.house.create(testData).then((house) => (
      result.resident.setHouse(house).then(() => {
        const houseData = house.toJSON()
        delete houseData.constructionId
        return checkExample(
          house.get('id'),
          models.house,
          url,
          `/${nameOfRoute}/${result.resident.get('id')}/house`,
          {
            virtualProperties: [
              'constructionId',
            ],
          },
          houseData,
          expect,
          true,
          true
        )
      })
    ))
  ))
}

/**
 * Test what happens when we try and insert multiple objects using the route
 *
 * @param {string} url Url of the server
 * @param {object} data Testdata of the file route
 * @param {object} models The sequelize models
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testBulkOutput(url, data, models, expect) {
  // slice to start from the 2nd element
  const dataList = prepareTestData(
    data[nameOfRoute].examples,
    data[nameOfRoute]
  ).slice(1)

  // insert a few works
  return insertMultiple(models[nameOfRoute], dataList)
  .then((result) => checkMultiple(
      result,
      url,
      `/${nameOfRoute}/`,
      data[nameOfRoute],
      expect
    ))
}

/**
 * Test what happens when we try and insert objects that don't validate
 *
 * @param {object} badExample Part of the testdata containg the bad data
 * @param {object} models The sequelize models
 * @param {function} expect Chai expect
 * @param {number} n The index of the bad example
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testBadExamplesNoPost(badExample, models, expect, n) {
  it(`should give descriptive errors #${n}`, () => (
    checkBadExampleNoPost(badExample, models[nameOfRoute], expect)
  ))
}

module.exports = function testResident(url, data, models, expect) {
  describe(nameOfRoute, () => {
    it('should get one instance', () => (
      testGetOne(url, data, models, expect)
    ))

    it('should add a instance', () => (
      testAdd(url, data, models, expect)
    ))

    it('should patch a instance', () => (
      testPatch(url, data, models, expect)
    ))

    describe('Should test subroutes', () => {
      it('should add the registerToken', () => (
        testAddRegisterTokenRelation(url, data, models, expect)
      ))
      it('should get the registerToken', () => (
        testGetRegisterTokenRelation(url, data, models, expect)
      ))
      it('should get the house', () => (
        testGetRegisterHouseRelation(url, data, models, expect)
      ))
    })

    it('should output all info', () => (
      testBulkOutput(url, data, models, expect)
    ))

    // test all bad examples
    data[nameOfRoute].badExamples.forEach((badExample, n) => {
      testBadExamplesNoPost(badExample, models, expect, n)
    })
  })
}
