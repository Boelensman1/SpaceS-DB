const request = require('supertest')

module.exports = function testAuthentication(url, data, models, expect) {
  it('should error about no authentication (route: /testAuth)', (done) => {
    request(url)
      .get('/testAuth')
      .set('Accept', 'application/json')
      .expect(401)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        const resp = res.body
        expect(resp).to.be.an('object')
        expect(resp.statusCode).to.be.equal(401)
        return done()
      })
  })

  it('should error about wrong authentication (route: /testAuth)', (done) => {
    request(url)
      .get('/testAuth')
      .set('Accept', 'application/json')
      .auth('unregisteredUser', 'testpassword')
      .expect(403)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        const resp = res.body
        expect(resp).to.be.an('object')
        expect(resp.statusCode).to.be.equal(403)
        return done()
      })
  })

  it('should login (route: /testAuth)', () => (
    // insert the admin
    models.admin.create(
      {
        userName: 'newUser',
        password: 'newPassword',
      }).then(() => {
        request(url)
          .get('/testAuth')
          .set('Accept', 'application/json')
          .auth('newUser', 'newPassword')
          .expect(200)
          .end((err, res) => {
            if (err) {
              throw err
            }
            const resp = res.body
            expect(resp).to.be.an('object')
            expect(resp.result).to.be.equal('OK-AUTH')

            // check with incorrect password
            request(url)
              .get('/testAuth')
              .set('Accept', 'application/json')
              .auth('newUser', 'wrongpassword')
              .expect(403)
              .end((error, result) => {
                if (error) {
                  throw err
                }
                const response = result.body
                expect(response).to.be.an('object')
                expect(response.statusCode).to.be.equal(403)
              })
          })
      })
  ))
}
