const request = require('supertest')
const http = require('http')

// comparison lib
const chai = require('chai')
chai.use(require('chai-datetime'))
chai.use(require('chai-things'))

chai.config.includeStack = true
const expect = chai.expect

const settings = require('config')

const host = 'localhost'
const port = parseInt(settings.server.port, 10) + 1
const url = `http://${host}:${port}`

describe('Test cors support', function testCors() {
  this.timeout(5000)

  before((done) => {
    require('../server.js').run(undefined, port)

    // make sure the server is started
    setTimeout(() => {
      request(url)
      .get('/test')
      .set('Accept', 'application/json')
      .end((err) => {
        if (err) {
          if (err.code === 'ECONNREFUSED') {
            return done(new Error('Server is not running.'))
          }
          return done(err)
        }
        return done()
      })
    }, 2500)
  })

  it('Should return which headers are allowed', (done) => {
    const requestOptions = {
      host,
      port,
      path: '/test',
      method: 'OPTIONS',
    }

      // send the request
    const req = http.request(requestOptions, (res) => {
      res.setEncoding('utf8')
        // for some reason it won't work without this piece of code
      res.on('data', (chunk) => {
      })

      const requiredKeys = [
        'access-control-allow-credentials',
        'access-control-allow-headers',
        'access-control-allow-methods',
        'access-control-allow-origin',
      ]

      expect(res.headers).to.contain.keys(requiredKeys)
      res.on('end', () => {
        done()
      })
    })

    req.on('error', (e) => {
      throw e
    })
    req.end()
  })
})
