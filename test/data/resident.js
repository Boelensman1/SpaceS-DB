module.exports = {
  virtualProperties: [
    'fullName',
  ],
  dbSpecificProperties: [
    'id',
    'createdAt',
    'updatedAt',
  ],
  dateProperties: [],
  examples: [
    {
      firstname: 'Wigger',
      snPrefix: null,
      surname: 'Boelens',
      email: 'test@test.com',
      email2: null,
      phoneNr: '+31612345678',
      phoneNr2: null,
    },
    {
      firstname: 'TestFirstName',
      snPrefix: 'Van De',
      surname: 'TestSurname',
      email: 'testfirstname.testsurname@gmail.com',
      email2: 'secondemail@gmail.com',
      phoneNr: '+31687654321',
      phoneNr2: '+31687899321',
    },
    {
      firstname: 'Thirdtestname',
      snPrefix: 'Den',
      surname: 'LongSurname'.repeat(3),
      email: 'somewhatWeirdEmail+Adress@gmail.co.uk',
      email2: null,
      phoneNr: '+31600000001',
      phoneNr2: '+31546169901',
    },
  ],
  badExamples: [
    {
      problems: {
        firstname: 'Is not allowed to be empty.',
        snPrefix: 'Should be between 1 and 15 characters.',
        surname: 'Should be between 1 and 35 characters.',
        email: 'Should be a valid email adress.',
        email2: 'Should be a valid email adress.',
        phoneNr: 'Should be a valid E.164 formatted phone number.',
        phoneNr2: 'Should be a valid E.164 formatted phone number.',
      },
      values: {
        firstname: undefined,
        surname: 'too long'.repeat(10),
        snPrefix: 'too long'.repeat(3),
        email: 'nonvalidemail',
        email2: 'nonvalidemail',
        phoneNr: 'nonvalid phonenumber',
        phoneNr2: 'nonvalid phonenumber',
      },
    },
  ],
}
