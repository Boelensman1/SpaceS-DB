const fs = require('fs')
const path = require('path')

const data = {}

/**
 * Add a datafile to the stack of datafiles
 *
 * @param {string} file The name of the file
 * @param {array} dataList The list to add the datafile to
 * @returns {undefined}
 */
function importData(file, dataList) {
  const name = path.basename(file, '.js')
  // eslint-disable-next-line import/no-dynamic-require
  dataList[name] = require(`./${name}`)
}

fs.readdirSync(__dirname).filter((file) => (file.indexOf('.') !== 0) && (file !== 'index.js')).forEach((file) => {
  importData(file, data)
})

module.exports = data
