module.exports = {
  dbSpecificProperties: [
    'id',
    'createdAt',
    'updatedAt',
    'residentId',
    'profilePictureId',
    'passwordHash',
  ],
  creationProperties: [
    'password',
  ],
  dateProperties: [],
  examples: [
    {
      username: 'TestForumAccountUser',
      password: 'abcDEFaa',
    },
    {
      username: 'TestForumAccountUser2',
      password: 'abcDEFaasdaf',
    },
    {
      username: 'Uname.123',
      password: 'passswordd',
    },
  ],
  badExamples: [
    {
      problems: {
        username: 'Does not comply to the username requirements.',
        password: 'Should be between 8 and 100 characters.',
      },
      values: {
        username: '..asdfljzx32o',
        password: 'abcDEFaa'.repeat(100),
      },
    },
  ],
}
