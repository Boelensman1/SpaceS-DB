module.exports = {
  dbSpecificProperties: [
    'id',
    'location',
    'createdAt',
    'updatedAt',
  ],
  dateProperties: [],
  examples: [
    {
      name: 'Profile icon testResident-{id} surname',
      uploadFrom: 'profilePicture.png',
      hash: 'fd52277d3128e514bcc9e852fa2bb6884aa96a7c',
      size: 2205,
      type: 'image/png',
      oldFileName: 'profilePicture.png',
    },
    {
      name: 'Profile icon testResident-{id} surname',
      uploadFrom: 'kitten.png',
      hash: '705fc26c7f3d65020a666b4d58cdd3af6ddb95e3',
      size: 907046,
      type: 'image/png',
      oldFileName: 'kitten.png',
    },
    {
      // name must be the same as the one below for the tests to work
      name: 'Profile icon testResident-{id} surname',
      uploadFrom: 'kitten2.png',
      hash: '3664860d780d65e4f84841e30cd084da0dce4508',
      size: 370407,
      type: 'image/png',
      oldFileName: 'kitten2.png',
    },
  ],
  badExamples: [
    {
      problems: {
        name: 'Cannot be empty.',
      },
      values: {
        name: '',
        uploadFrom: 'kitten.png',
      },
    },
    {
      problems: {
        name: 'Should be between 3 and 200 characters.',
      },
      values: {
        name: 'ab',
        uploadFrom: 'kitten.png',
      },
    },
  ],
}
