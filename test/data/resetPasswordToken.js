module.exports = {
  dbSpecificProperties: [
    'createdAt',
    'updatedAt',
    'token',
    'accountId',
  ],
  dateProperties: [],
  examples: [{}, {}, {}],
}
