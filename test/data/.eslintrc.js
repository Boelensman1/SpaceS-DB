'use strict';

module.exports = {
  'rules': {
    'max-len': [1, 120, 4],
  }
};
