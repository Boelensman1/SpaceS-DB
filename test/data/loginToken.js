module.exports = {
  dbSpecificProperties: [
    'id',
    'createdAt',
    'updatedAt',
    'token',
    'accountId',
  ],
  dateProperties: [],
  examples: [{}, {}, {}],
}
