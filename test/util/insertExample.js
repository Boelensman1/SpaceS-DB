const q = require('q')
const request = require('supertest')

/**
 * Insert a example object into the database using the api
 *
 * @param {object} testData Data to be inserted
 * @param {string} url Url of the server
 * @param {string} suburl Url of the route where the data should be send to
 * @param {regex} locationExpect Regex describing what the location header
 *                               should look like
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done and rejects on error
 */
function insertExample(testData, url, suburl, locationExpect, expect) {
  return new q.Promise((resolve, reject, notify) => {
    request(url)
    .post(suburl)
    .send(testData)
    .set('Accept', 'application/json')
    .auth('testuser', 'testpassword')
    .expect(201)
    .expect('Location', locationExpect)
    .end((err, res) => {
      if (err) {
        return reject(err)
      }

      const location = res.headers.location

      const re = /\/(\w*?)$/
      const id = re.exec(location)[1]

      return resolve(id)
    })
  })
}

module.exports = insertExample
