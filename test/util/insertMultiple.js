const q = require('q')

/**
 *  Insert multiple examples for testing
 *
 * @param {object} model The model that we insert instances of
 * @param {object} data The testdata
 * @returns {Promise} Promise that resolves to a array
 *                    containing the inserted objects
 */
function insertMultiple(model, data) {
  const promises = []
  // insert
  data.forEach((element) => {
    promises.push(
      model.create(element)
    )
  })

  // wait for everything to be inserted
  return q.all(promises)
}

module.exports = insertMultiple
