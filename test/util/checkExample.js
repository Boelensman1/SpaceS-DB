const sanitize = require('../util/sanitize')
const getSanitizedExample = require('../util/getSanitizedExample')
const checkByRequesting = require('./checkByRequesting')

/**
 * Check an example that has just been inserted
 *
 * @param {number} id Id of the just inserted object that should be checked
 * @param {object} model Model of the just object to check
 * @param {string} url Url of server
 * @param {string} suburl Url of the route
 * @param {object} data Testdata of the inserted model
 * @param {object} testData The data that was inserted
 * @param {function} expect Chai expect
 * @param {boolean} checkByRequest Wether to also check by doing a request
 * @param {boolean} staticSubUrl Wether not to add the id at the end of the url
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function checkExample(
  id,
  model,
  url,
  suburl,
  data,
  testData,
  expect,
  checkByRequest,
  staticSubUrl
) {
  // check if work is created
  // eslint-disable-next-line consistent-return
  return model.findById(id).then((result) => {
    expect(result).to.not.be.null
    let dataTest = result.toJSON()

    // test for the database specific atttibutes
    const DBSpecificProps = data.DBSpecificProperties
    if (DBSpecificProps) {
      expect(dataTest).to.include.keys(DBSpecificProps)
    }

    // remove the database-specific attributes
    dataTest = sanitize(dataTest, data)

    const testDataSanitized = getSanitizedExample(testData, data)

    expect(dataTest).to.eql(testDataSanitized)

    if (checkByRequest) {
      if (!staticSubUrl) {
        suburl += `/${id}`
      }
      return checkByRequesting(url, suburl, data, testData, expect)
    }
  })
}

module.exports = checkExample
