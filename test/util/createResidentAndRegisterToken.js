const q = require('q')
const randomstring = require('randomstring')
/**
 * Create a resident and a registerToken for use in tests
 *
 * @param {object} models The sequelize models
 * @returns {Promise} Promise that resolves into a object containing an
 *                    id, the resident and its register token in the form of
 *                    {id: string, resident: object, registerToken: object}
 */
function createResidentAndRegisterToken(models) {
  const id = randomstring.generate(7)
  const residentData = {
    firstname: `testResident-${id}`,
    surname: 'surname',
    initials: 'T.S.',
    email: 'testResident@test.com',
  }

  const data = {}
  const createModels = [
    models.resident.create(residentData),
    models.registerToken.create(),
  ]

  return q.all(createModels).then((results) => {
    data.id = id
    data.resident = results[0]
    data.registerToken = results[1]

    return data.resident.setRegisterToken(data.registerToken).then(
      () => (data),
      (err) => { throw err }
    )
  }, (err) => { throw err })
}

module.exports = createResidentAndRegisterToken
