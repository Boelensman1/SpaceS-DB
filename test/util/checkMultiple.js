const request = require('supertest')
const sanitize = require('../util/sanitize')
const getSanitizedExample = require('../util/getSanitizedExample')
const q = require('q')

/**
 * Checks if the inserting of the multiple objects has been done
 * correctly and succesfully
 *
 * @param {array} insertedObjects The objects that have been inserted
 * @param {string} url Url of the server
 * @param {string} suburl Url to the route we're testing
 * @param {object} data The testdata
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function checkMultiple(insertedObjects, url, suburl, data, expect) {
  return new q.Promise((resolve, reject, notify) => {
    request(url)
    .get(suburl)
    .set('Accept', 'application/json')
    .auth('testuser', 'testpassword')
    .expect(200)
    .end((err, res) => {
      if (err) {
        reject(err)
        return
      }

      const insertedIds = []
      insertedObjects.forEach((insertedObject) => {
        insertedIds.push(insertedObject.get('id'))
      })

      const resp = res.body
      expect(resp).to.have.length.above(1)

      let count = 0
      resp.forEach((object) => {
        if (insertedIds.indexOf(object.id) > -1) {
          const dataTest = sanitize(object, data)
          const examples = data.examples.map((example) => (
            getSanitizedExample(example, data)
          ))
          try {
            // should be the reverse, but don't know how
            expect(examples).to.include.something.that.eql(dataTest)
          } catch (error) {
            /* eslint-disable no-console */
            console.log('\n')
            console.dir(dataTest)
            console.log('\n should be in: \n')
            console.dir('dataList', data.examples)
            console.log('\n')
            /* eslint-enable */
            throw error
          }
          count += 1
        }
      })

      expect(count).to.be.above(1)

      resolve()
    })
  })
}

module.exports = checkMultiple
