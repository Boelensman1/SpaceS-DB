/**
 * Sanitize a object that we get back from the database
 *
 * @param {object} object The object that sequelize returns
 * @param {object} data The testdata belonging to the model that
 *                      the object belongs to
 * @returns {object} The sanitized object
 */
function sanitize(object, data) {
  const dbSpecificProperties = data.dbSpecificProperties
  const virtualProperties = data.virtualProperties
  const dateProperties = data.dateProperties || []

  // remove the database-specific attributes
  if (dbSpecificProperties) {
    dbSpecificProperties.forEach((key) => {
      delete object[key]
    })
  }

  if (virtualProperties) {
    virtualProperties.forEach((key) => {
      delete object[key]
    })
  }

  // convert the date's
  dateProperties.concat(['updatedAt', 'createdAt']).forEach((key) => {
    if ({}.hasOwnProperty.call(object, key)) {
      object[key] = new Date(object[key])
    }
  })

  return object
}

module.exports = sanitize
