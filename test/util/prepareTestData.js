// makes sure none of the data is changed,

/**
 * Makes the properties that are defined as dataproperties into dates
 *
 * @param {object} object the object that has properties that should be date's
 * @param {object} newObject the object to which the date's should be added
 * @param {array} dateProperties List of the dateproperties
 * @returns {object} The newObject with its dateproperties as date's
 */
function convertDateProperties(object, newObject, dateProperties) {
  // convert the date's back
  dateProperties.forEach((key) => {
    newObject[key] = object[key]
  })

  return newObject
}

/**
 * Create a clone of the testData and make sure that it has the proper types
 * At the moment this only means that its date's are converted into date objects
 *
 * @param {object|array} object The object to prepareTestData or an array of
 *                              objects to prepareTestData
 * @param {object} data The data belonging to this testData
 * @returns {object} The object or the array of objects
 */
function prepareTestData(object, data) {
  const dateProperties = data.dateProperties

  let newObject = JSON.parse(JSON.stringify(object))

  if (Array.isArray(object)) {
    Object.keys(newObject).forEach((key) => {
      const instance = object[key]
      convertDateProperties(instance, newObject[key], dateProperties)
    })
  } else {
    newObject = convertDateProperties(object, newObject, dateProperties)
  }

  return newObject
}

module.exports = prepareTestData
