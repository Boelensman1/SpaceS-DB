const handleValidationErrors =
  require('../../api/helpers/util/handleValidationErrors')

/**
 * Format validation errors as they would be formatedd by a route
 *
 * @param {array} err The errors
 * @returns {object} The formatted errors
 */
function formatErrors(err) {
  const emptyFunc = () => {}
  const res = { status: () => ({ send: emptyFunc }) }
  const logger = { info: emptyFunc }
  return handleValidationErrors(err, res, logger)
}

/**
 * Try to insert a bad example and check if the returned errors are correct
 *
 * @param {object} badExample The bad example to try and insert
 * @param {object} model The model that we insert instances of
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done and rejects on error
 */
function checkBadExampleNoPost(badExample, model, expect) {
  return model.build(badExample.values).validate().then((err) => {
    const formattedErros = formatErrors(err)
    expect(formattedErros).to.deep.equal(badExample.problems)
  })
}

module.exports = checkBadExampleNoPost
