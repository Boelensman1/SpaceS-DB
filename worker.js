const authParser = require('express-auth-parser')

/**
 * Set up server
 *
 * @param {object} swaggerExpress The swagger object
 * @param {object} logger Instance of the logger
 * @param {object} auditLogger Instance of the auditLogger
 * @param {function} cb Callback
 * @returns {undefined}
 */
function createServer(swaggerExpress, logger, auditLogger, cb) {
  const server = require('express')()

  if (logger) {
    const expressLogger = logger.child({ type: 'express' })

    server.use(require('express-bunyan-logger')({
      logger: expressLogger,
    }))
  }
  server.use(authParser)
  server.use((req, res, next) => {
    req.files = { file: null }
    next()
  })

  swaggerExpress.register(server)


  cb(server)
}

exports.createServer = createServer
